// Authentication needs a Private key to sign API_KEYs

package server

import (
    "bytes"
    "crypto/ed25519"
    "crypto/rand"
    "encoding/base64"
    "fmt"
    "net/http"
    "os"
)

const (
    HEADER_API_KEY="X-PDF-API-KEY"
    TokenSize=8
)

var (
    publicKey ed25519.PublicKey
)

type AuthHandler struct {
    mux *http.ServeMux
}

func init(){
    if k:=os.Getenv("PUBLIC_KEY"); k==""{
        println("NO USERS WILL BE AUTHENTICATED. NO KEY IS PRESENT")
    }else{
        pk, err:=_unarmor([]byte(k))
        if err!=nil{
            panic(err)
        }
        publicKey=ed25519.PublicKey(pk)
    }
}

func Authenticate(apiKey []byte) (ok bool){
    spl:=bytes.Split(apiKey, []byte{'.'})
    if len(spl)!=2{
        return
    }
    token, err:=_unarmor(spl[0])
    if err!=nil{
        log.Warnf("Error decoding token: %s\n", err)
        return
    }
    sig, err:=_unarmor(spl[1])
    if err!=nil{
        log.Warnf("Error decoding signature: %s\n", err)
        return
    }
    ok=ed25519.Verify(publicKey, token, sig)
    log.Debugf("Verification for %s returned %t\n", apiKey, ok)
    return
}

func (h *AuthHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request){
    key:=req.Header.Get(HEADER_API_KEY)
    if key=="" || !Authenticate([]byte(key)){
        http.Error(rw, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
        return
    }
    h.mux.ServeHTTP(rw, req)
}

func GeneratePrivateKey() (armoredKey string, err error){
    var seed [ed25519.SeedSize]byte
    _,err=rand.Read(seed[:])
    if err!=nil{
        return
    }
    pk:=ed25519.NewKeyFromSeed(seed[:])
    armoredKey=base64.StdEncoding.EncodeToString(pk)
    return
}

func GetPubkey(pka string) (publicKeyArmored string, err error){
    privateKey, err:=unarmor(pka)
    if err!=nil{
        return
    }
    publicKey:=privateKey.Public().(ed25519.PublicKey)
    publicKeyArmored=base64.StdEncoding.EncodeToString(publicKey)
    return
}

func GenerateAPIKey(privateKeyArmored string) (apiKey string, err error){
    privateKey, err:=unarmor(privateKeyArmored)
    if err!=nil{
        return
    }
    var token [TokenSize]byte
    _, err=rand.Read(token[:])
    if err!=nil{
        return
    }
    log.Debugf("Token is %x\n", token)
    signature:=ed25519.Sign(privateKey, token[:])
    if err!=nil{
        return
    }
    apiKey=fmt.Sprintf("%s.%s", base64.StdEncoding.EncodeToString(token[:]), base64.StdEncoding.EncodeToString(signature))
    return
}

func unarmor(armoredKey string) (unarmored ed25519.PrivateKey, err error){
    return _unarmor([]byte(armoredKey))
}

func _unarmor(armoredThing []byte) (unarmored []byte, err error){
    unarmored=make([]byte, base64.StdEncoding.DecodedLen(len(armoredThing)))
    finalLen, err:=base64.StdEncoding.Decode(unarmored, []byte(armoredThing))
    unarmored=unarmored[:finalLen]
    return
}
