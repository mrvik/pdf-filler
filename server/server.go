package server

import (
    "context"
    "gitlab.com/mrvik/pdf-filler/lg"
    "net"
    "net/http"
    "os"
    "sync"
    "time"
)

var log=lg.CreateLogger("server")

func InitializeServer(ctx context.Context, wg *sync.WaitGroup){
    mux:=createMux()
    server:=&http.Server{
        Addr: getListerAddr(),
        Handler: &AuthHandler{
            mux: mux,
        },
        BaseContext: rtcontext(ctx),
    }
    wg.Add(1)
    go startServer(ctx, server, wg);
}

func startServer(ctx context.Context, server *http.Server, wg *sync.WaitGroup){
    defer wg.Done()
    erch:=make(chan error)
    go func(ch chan<- error){
        err:=server.ListenAndServe()
        ch<-err
    }(erch)
    log.Infof("Server started and listening on %q\n", server.Addr)
    select{
    case <-ctx.Done():
        tctx,cancel:=context.WithTimeout(context.Background(), time.Second*2)
        defer cancel()
        server.Shutdown(tctx)
    case err:=<-erch:
        if err!=http.ErrServerClosed{
            panic(err)
        }
    }
}

func getListerAddr() (addr string){
    if port:=os.Getenv("PORT"); port!=""{
        addr=":"+port
    }
    return
}

func rtcontext(ctx context.Context) func(net.Listener) context.Context{
    return func(net.Listener) context.Context{
        return ctx
    }
}
