package server

import (
    "gitlab.com/mrvik/pdf-filler/filler"
    "gitlab.com/mrvik/pdf-filler/poll"
    "net/http"
)

var handlers=map[string]http.Handler{
    "/fill": filler.Handler(0),
    "/poll/": poll.Handler(0),
}

func createMux() *http.ServeMux{
    mux:=new(http.ServeMux)
    for route,handler:=range(handlers){
        mux.Handle(route, handler)
    }
    return mux
}
