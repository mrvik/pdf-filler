package poll

import (
    "bytes"
    "context"
    "encoding/hex"
    "errors"
    "gitlab.com/mrvik/pdf-filler/lg"
    "golang.org/x/sync/semaphore"
    "math/rand"
    "runtime"
    "sync"
    "time"
)

const (
    //Unclaimed jobs will be removed after a minute
    GC_TRESHOLD=time.Minute
)

var (
    //Set this from main. Works as a global cancel signal
    GlobalContext context.Context
    jobs=&mutexedMap{
        mp: make(map[string]*Job),
        mtx: new(sync.RWMutex),
    }
    log=lg.CreateLogger("farm")
    jobsSemaphore=semaphore.NewWeighted(4)
)

//Errors
var (
    ErrNotFound=errors.New("selected token cannot be found")
)

type mutexedMap struct{
    mp map[string]*Job
    mtx *sync.RWMutex
}

func (mm mutexedMap) Has(token string) bool{
    mm.mtx.RLock()
    defer mm.mtx.RUnlock()
    _,ok:=mm.mp[token]
    return ok
}

func (mm mutexedMap) register(token string, job *Job){
    if mm.Has(token){
        panic("token already exists in map")
    }
    mm.mtx.Lock()
    defer mm.mtx.Unlock()
    mm.mp[token]=job
}

func (mm mutexedMap) gc(wg *sync.WaitGroup){
    defer wg.Done()
    mm.mtx.RLock()
    var removeTokens []string
    for token,job:=range(mm.mp){
        if job.ctx.Err()==nil{
            continue
        }
        job.mtx.Lock()
        if time.Since(job.finishedAt)>GC_TRESHOLD {
            removeTokens=append(removeTokens, token)
        }
        job.mtx.Unlock()
    }
    mm.mtx.RUnlock()
    if len(removeTokens)==0{
        return
    }
    log.Infof("Removing %d stale jobs\n", len(removeTokens))
    mm.mtx.Lock()
    defer mm.mtx.Unlock()
    for _,token:=range(removeTokens){
        delete(mm.mp, token)
    }
}

func (mm mutexedMap) FetchJob(ctx context.Context, token string) (result *bytes.Buffer, err error){
    mm.mtx.RLock()
    job, ok:=mm.mp[token]
    mm.mtx.RUnlock()
    if !ok{
        err=ErrNotFound
        return
    }
    var wg sync.WaitGroup
    wg.Add(1)
    go mm.gc(&wg)
    defer wg.Wait()
    select{
    case <-ctx.Done():
        err=ctx.Err() //ctx expired before result was ready
    case <-job.ctx.Done():
        job.mtx.Lock()
        result, err=job.result, job.err
        job.mtx.Unlock()
        mm.removeJob(token)
    }
    return
}

func (mm mutexedMap) removeJob(token string){
    mm.mtx.Lock()
    defer mm.mtx.Unlock()
    delete(mm.mp, token)
    runtime.GC() //Force garbage collection after job has been removed (with the output buffer)
}

type StartFunc func(ctx context.Context) (result *bytes.Buffer, err error)

type Job struct{
    ctx context.Context
    cancel context.CancelFunc
    mtx sync.Mutex
    result *bytes.Buffer
    err error
    finishedAt time.Time
}

func AddJob(init StartFunc) (token string){
    token=generateToken()
    ctx, cancel:=context.WithCancel(GlobalContext)
    job:=&Job{
        ctx: ctx,
        cancel: cancel,
    }
    jobs.register(token, job)
    go startWorking(init, job)
    return
}

func _generateToken() string{
    var rdb [32]byte
    _,err:=rand.Read(rdb[:])
    if err!=nil{
        panic(err)
    }
    return hex.EncodeToString(rdb[:])
}

func generateToken() (candidate string){
    for{
        candidate=_generateToken()
        if !jobs.Has(candidate){
            break
        }
    }
    return candidate
}

func startWorking(initfn StartFunc, job *Job){
    defer recoverJob(job)
    var (
        res *bytes.Buffer
        err=jobsSemaphore.Acquire(job.ctx, 1)
    )
    if err==nil{
        defer jobsSemaphore.Release(1)
        defer job.cancel()
        res, err=initfn(job.ctx)
    }
    job.mtx.Lock()
    defer job.mtx.Unlock()
    job.finishedAt=time.Now()
    job.result, job.err=res, err
}

func recoverJob(job *Job){
    var e interface{}
    if e=recover(); e==nil{
        return
    }
    if job.cancel!=nil{
        defer job.cancel()
    }
    switch e.(type){
    case error, string:
        log.Errorf("Panic on Job: %s\n", e)
    default:
        log.Errorf("Unknown type %T on panic. Error is %v\n", e, e)
    }
}
