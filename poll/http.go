package poll

import (
    "context"
    "net/http"
    "path"
    "time"
)

type Handler byte

func (Handler) ServeHTTP(rw http.ResponseWriter, req *http.Request){
    token:=path.Base(req.URL.Path)
    if len(token)<2{
        http.Error(rw, "bad path", http.StatusBadRequest)
        return
    }
    if !jobs.Has(token){
        http.Error(rw, "token has no job associated", http.StatusNotFound)
        return
    }
    ctx, cancel:=context.WithTimeout(req.Context(), time.Second*8)
    defer cancel()
    res, err:=jobs.FetchJob(ctx, token)
    switch err{
    case nil:
    case ErrNotFound:
        http.Error(rw, err.Error(), http.StatusNotFound)
        return
    case context.DeadlineExceeded:
        rw.WriteHeader(http.StatusNoContent) //Nothing to see here
        return
    default:
        http.Error(rw, err.Error(), http.StatusInternalServerError)
        return
    }
    res.WriteTo(rw)
}
