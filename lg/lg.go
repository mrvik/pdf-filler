//Create loggers for packages

package lg

import (
    "gitlab.com/mrvik/logger/log"
)

var debug bool

func CreateLogger(pkg string) *log.Lgr{
    if debug{
        return log.CreateDebug(pkg, nil, nil)
    }
    return log.Create(pkg, nil, nil)
}
