package main

import (
    "context"
    "flag"
    "gitlab.com/mrvik/pdf-filler/lg"
    "gitlab.com/mrvik/pdf-filler/server"
    "gitlab.com/mrvik/pdf-filler/poll"
    "os"
    "os/signal"
    "sync"
    "syscall"
)

var log=lg.CreateLogger("main")

func main(){
    var (
        privateKeyPath string
        generatePrivateKey, printPublicKey bool
    )
    flag.StringVar(&privateKeyPath, "private-key", "", "Set private key to use and generate a new API Key. File must exist unless 'generate' is specified")
    flag.BoolVar(&generatePrivateKey, "generate", false, "Generate a private key and write it to the path pointed by -private-key. Implies 'print-public'")
    flag.BoolVar(&printPublicKey, "print-public", false, "Print public key from specified private key")
    flag.Parse()
    if privateKeyPath==""{
        apiInit()
        return
    }
    keyOptions(privateKeyPath, generatePrivateKey, printPublicKey)
}

func apiInit() {
    var (
        wg sync.WaitGroup
        ctx, stopFn=context.WithCancel(context.Background())
    )
    defer stopFn()
    poll.GlobalContext=ctx
    server.InitializeServer(ctx, &wg)
    watchSignals(ctx, stopFn)
    log.Info("Waiting for goroutines to terminate")
    wg.Wait()
}

func watchSignals(ctx context.Context, stopFn context.CancelFunc){
    defer stopFn()
    channel:=make(chan os.Signal, 1)
    signal.Notify(channel, syscall.SIGTERM, syscall.SIGINT)
    loop:
    for{
        select{
        case sig,ok:=<-channel:
            if !ok{
                break loop
            }
            switch sig{
            case syscall.SIGTERM, syscall.SIGINT:
                break loop
            }
        case <-ctx.Done():
            break loop
        }
    }
}
