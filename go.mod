module gitlab.com/mrvik/pdf-filler

go 1.15

require (
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	gitlab.com/mrvik/logger v0.0.0-20200624181218-1870fc7f22c4
	gitlab.com/mrvik/unipdf/v3 v3.9.0
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	golang.org/x/sys v0.0.0-20200905004654-be1d3432aa8f // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
