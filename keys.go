package main

import (
    "gitlab.com/mrvik/pdf-filler/server"
    "io/ioutil"
)

func keyOptions(privateKeyPath string, generatePrivateKey, printPublicKey bool){
    var (
        privateKey string
        err error
    )
    if generatePrivateKey{
        privateKey, err=server.GeneratePrivateKey()
        if err!=nil{
            panic(err)
        }
        if err=writeKey(privateKeyPath, []byte(privateKey)); err!=nil{
            panic(err)
        }
    }else{
        if pka, err:=readKey(privateKeyPath); err!=nil{
            panic(err)
        }else{
            privateKey=string(pka)
        }
    }
    if printPublicKey || generatePrivateKey{
        pk, err:=server.GetPubkey(privateKey)
        if err!=nil{
            panic(err)
        }
        log.Infof("Public key: %s\n", pk)
    }
    apiKey, err:=server.GenerateAPIKey(privateKey)
    if err!=nil{
        panic(err)
    }
    log.Infof("Generated API Key: %s\n", apiKey)
}

func readKey(path string) (armoredKey []byte, err error){
    armoredKey, err=ioutil.ReadFile(path)
    return
}

func writeKey(path string, armoredKey []byte) (err error){
    err=ioutil.WriteFile(path, armoredKey, 0600)
    return
}
