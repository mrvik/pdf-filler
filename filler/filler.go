package filler

import (
    "bytes"
    "context"
    "gitlab.com/mrvik/unipdf/v3/annotator"
    "gitlab.com/mrvik/unipdf/v3/fjson"
    "gitlab.com/mrvik/unipdf/v3/model"
    "io"
    "sync"
)

var (
    //Seems like unipdf has internal race conditions and the FlattenFields call must be serialized.
    flattenMutex=new(sync.Mutex)
)

//Each array holds the info to fill the pdf-file. This array holds objects with name and value string fields.
//  [
//      {
//          "name": "Field name",
//          "value": "Field value"
//      }
//  ]
//
//Each of them bust be mashaled and base64-encoded (Go JSON marshaler does this by default when encoding []byte)
type IncomingData [][]byte

func FillPDF(ctx context.Context, pdfFile []byte, data IncomingData) (output *bytes.Buffer, err error){
    var (
        filePages=make([][]*model.PdfPage, len(data))
        errorChannel=make(chan error, 2)
    )
    log.Debug("PDF filling has started")
    log.Debugf("Data available for %d files\n", len(data))
    for key,pageData:=range(data){
        go fillFile(ctx, &filePages[key], bytes.NewReader(pdfFile), pageData, errorChannel)
    }
    for range(data){
        select{
        case <-ctx.Done():
            err=ctx.Err()
            return
        case err=<-errorChannel:
            if err!=nil{
                return
            }
        }
    }
    log.Debug("Merging all pages into the final PDF file")
    //Recap with all pages
    pdfWriter:=model.NewPdfWriter()
    for _,pp:=range(filePages){
        for _,page:=range(pp){
            err=pdfWriter.AddPage(page)
            if err!=nil{
                return
            }
        }
    }
    log.Debug("Writing final file to output")
    output=new(bytes.Buffer)
    err=pdfWriter.Write(output)
    return
}

//Here is where the hard work is made. This function is ready to be executed on a separate goroutine for each file to be filled
func fillFile(ctx context.Context, pages *[]*model.PdfPage, pdfFile io.ReadSeeker, data []byte, errorChannel chan<- error){
    handle, err:=model.NewPdfReaderLazy(pdfFile)
    if err!=nil{
        log.Warnf("Error reading PDF file: %s\n", err)
        errorChannel<-err
        return
    }
    dataSource, err:=fjson.LoadFromJSON(bytes.NewReader(data))
    if err!=nil{
        errorChannel<-err
        return
    }
    err=handle.AcroForm.Fill(dataSource)
    if err!=nil{
        errorChannel<-err
        return
    }
    if err=ctx.Err(); err!=nil{
        errorChannel<-err
        return
    }
    flattenMutex.Lock()
    log.Debug("Flattening fields")
    err=handle.FlattenFields(true, annotator.FieldAppearance{})
    flattenMutex.Unlock()
    log.Debug("Finished flattening fields")
    if err!=nil{
        errorChannel<-err
        return
    }
    *pages=handle.PageList
    errorChannel<-nil
}

