package filler

import (
    "bytes"
    "context"
    "encoding/json"
    "fmt"
    "gitlab.com/mrvik/pdf-filler/lg"
    "gitlab.com/mrvik/pdf-filler/poll"
    "net/http"
)

var (
    log=lg.CreateLogger("filler")
)

type BodyContent struct{
    PDFFile []byte
    FillData IncomingData
}

type Handler byte

func (Handler) ServeHTTP(rw http.ResponseWriter, req *http.Request){
    if req.Method!=http.MethodPost{
        doError(rw, http.StatusBadRequest)
        return
    }
    var content BodyContent
    reader:=json.NewDecoder(req.Body)
    err:=reader.Decode(&content)
    if err!=nil || content.PDFFile==nil || content.FillData==nil{
        http.Error(rw, err.Error(), http.StatusBadRequest)
        return
    }
    req.Body.Close()
    fn:=func(ctx context.Context) (res *bytes.Buffer, err error){
        res, err=FillPDF(ctx, content.PDFFile, content.FillData)
        if err!=nil{
            log.Warnf("Error processing request: %s\n", err)
        }
        return
    }
    token:=poll.AddJob(fn)
    rw.WriteHeader(http.StatusAccepted)
    rw.Write([]byte(token))
}

func doError(rw http.ResponseWriter, errorCode int){
    http.Error(rw, fmt.Sprintf("%q", http.StatusText(errorCode)), errorCode)
}
